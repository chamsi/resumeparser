package com.approch.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
@RestController
public class ResumeProcessor {
//first approach will work only if the cv is organized
	
	@PostMapping("/pdf/text")
	public List<String> extractTextFromPDF(@RequestParam("file") MultipartFile file) throws IOException {
		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);

			CustomPDFTextStripper stripper = new CustomPDFTextStripper();
			String text = stripper.getText(document);
			document.close();

			List<String> sections = stripper.getSections();
			System.out.println(text);
			System.out.println(sections);
			return sections;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	 @PostMapping("/extractSkills")
	    public String extractSkills(@RequestParam("file") MultipartFile file) throws IOException {
	       // String  skills = PDFSectionExtractor.extractSkills(file);
	        //String  Experience = PDFSectionExtractor.extractExperience(file);
		  String  Certifs = PDFSectionExtractor.extractCertifications(file);
	        return Certifs;
	    }

}
