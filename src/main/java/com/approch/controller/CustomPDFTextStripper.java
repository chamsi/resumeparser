package com.approch.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

public class CustomPDFTextStripper  extends PDFTextStripper{
	private static final float LARGE_FONT_SIZE_THRESHOLD = 14;
	private boolean inLargeFont = false;
    private StringBuilder sectionBuilder = new StringBuilder();
    private List<String> sections = new ArrayList<>();
	public CustomPDFTextStripper() throws IOException {
		super();
	
	}
	 @Override
	    protected void startPage(PDPage page) throws IOException {
	        super.startPage(page);
	        inLargeFont = false;
	    }

	    @Override
	    protected void endPage(PDPage page) throws IOException {
	        super.endPage(page);
	        if (inLargeFont) {
	            // End of a section
	            sections.add(sectionBuilder.toString().trim());
	            sectionBuilder = new StringBuilder();
	            inLargeFont = false;
	        }
	    }

	    @Override
	    protected void processTextPosition(TextPosition text) {
	        super.processTextPosition(text);
	        float fontSize = text.getFontSize();
	        if (fontSize >= LARGE_FONT_SIZE_THRESHOLD) {
	            if (!inLargeFont) {
	                // Start of a new section
	                sections.add(sectionBuilder.toString());
	                sectionBuilder = new StringBuilder();
	                inLargeFont = true;
	            }
	        } else {
	            if (inLargeFont) {
	                // End of a section
	                sections.add(sectionBuilder.toString());
	                sectionBuilder = new StringBuilder();
	                inLargeFont = false;
	            }
	        }

	        sectionBuilder.append(text.getUnicode());
	    }

	    public List<String> getSections() {
	    if (sectionBuilder.length() > 0) {
	            // End of a section
	            sections.add(sectionBuilder.toString());
	            sectionBuilder = new StringBuilder();
	            inLargeFont = false;
	        }
	        return sections;
	    }

}
