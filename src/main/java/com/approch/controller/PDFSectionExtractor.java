package com.approch.controller;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfileReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PDFSectionExtractor {

	
	public static String extractExperience(MultipartFile file) throws IOException {
		String experienceSection = null;
		String regex;
		String regexLowerCased;
		Pattern pattern;
		Pattern pattern2;

		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);

			PDFTextStripper pdfStripper = new PDFTextStripper();
			pdfStripper.setSortByPosition(true);
			pdfStripper.setStartPage(0);
			pdfStripper.setEndPage(document.getNumberOfPages());
			pdfStripper.setLineSeparator("\n");
			pdfStripper.setWordSeparator(" ");
			pdfStripper.setShouldSeparateByBeads(false);

			String text = pdfStripper.getText(document);
			System.out.println(text);
			String lang = detectLanguage(text);

			if (lang.equals("en")) {
				// Search for the "experience" section using regular expressions
				regex = "(?s)EXPERIENCE.*?(?:(?=\\s*LANGUAGES)|(?=\\s*LICENSES\\s&\\sCERTIFICATIONS)|(?=\\s*CERTIFICATES)|(?=\\s*EDUCATION)|(?=\\s*EXPERIENCE)|(?=\\s*PROJECTS)|$)";
				regexLowerCased = "(?s)Experience.*?(?:(?=\\s*Languages)|(?=\\s*Licenses\\s&\\sCertifications)|(?=\\s*Certificates)|(?=\\s*Education)|(?=\\s*Experience)|(?=\\s*Projects)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);

			} else {

				regex = "(?s)EXPÉRIENCE.*?(?:(?=\\s*COORDONNÉES)|(?=\\s*CERTIFICATIONS)|(?=\\s*PRINCIPALES\\sCOMPÉTENCES)|(?=\\s*RÉSUMÉ)|(?=\\s*FORMATION)|(?=\\s*LANGUAGES)|(?=\\s*EXPÉRIENCE)|(?=\\s*PROJETS)|$)";
				regexLowerCased = "(?s)Expérience.*?(?:(?=\\s*Coordonnées)|(?=\\s*Certifications)|(?=\\s*Principales\\scompétences)|(?=\\s*Résumé)|(?=\\s*Formation)|(?=\\s*Languages)|(?=\\s*Expérience)|(?=\\s*Projets)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);

			}
			Matcher matcher = pattern.matcher(text);
			Matcher matcher2 = pattern2.matcher(text);

			if (matcher.find()) {
				experienceSection = matcher.group();
			} else if (matcher2.find()) {
				experienceSection = matcher2.group();
			}

			document.close();
			return experienceSection;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String extractSkills(MultipartFile file) throws IOException {
		String skillSection = null;
		String regex;
		String regexLowerCased;
		Pattern pattern;
		Pattern pattern2;

		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);

			// Get the text content of the PDF
			PDFTextStripper pdfStripper = new PDFTextStripper();
			pdfStripper.setSortByPosition(true);
			pdfStripper.setStartPage(0);
			pdfStripper.setEndPage(document.getNumberOfPages());
			pdfStripper.setLineSeparator("\n");
			pdfStripper.setWordSeparator(" ");
			pdfStripper.setShouldSeparateByBeads(false);

			String text = pdfStripper.getText(document);
			System.out.println(text);
			String lang = detectLanguage(text);

			if (lang.equals("en")) {
				regex = "(?s)SKILLS.*?(?:(?=\\s*LANGUAGES)|(?=\\s*LICENSES\\s&\\sCERTIFICATIONS)|(?=\\s*CERTIFICATES)|(?=\\s*EDUCATION)|(?=\\s*EXPERIENCE)|(?=\\s*PROJECTS)|$)";
				regexLowerCased = "(?s)Skills.*?(?:(?=\\s*Languages)|(?=\\s*Licenses\\s&\\sCertifications)|(?=\\s*Certificates)|(?=\\s*Education)|(?=\\s*Experience)|(?=\\s*Projects)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			} else {
				regex = "(?s)COMPÉTENCES(?:\\s*TECHNIQUES)?.*?(?:(?=\\s*COORDONNÉES)|(?=\\s*CERTIFICATIONS)|(?=\\s*RÉSUMÉ)|(?=\\s*FORMATION)|(?=\\s*LANGUAGES)|(?=\\s*EXPÉRIENCE)|(?=\\s*PROJETS)|$)";
				regexLowerCased = "(?s)Compétences(?:\\s*Techniques)?.*?(?:(?=\\s*Coordonnées)|(?=\\s*Certifications)|(?=\\s*Résumé)|(?=\\s*Formation)|(?=\\s*Languages)|(?=\\s*Expérience)|(?=\\s*Projets)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			}

			Matcher matcher = pattern.matcher(text);
			Matcher matcher2 = pattern2.matcher(text);

			if (matcher.find()) {
				skillSection = matcher.group();
			} else if (matcher2.find()) {
				skillSection = matcher2.group();
			}

			document.close();

			return skillSection;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String extractEducation(MultipartFile file) throws IOException {
		String EducationSection = null;
		String regex;
		String regexLowerCased;
		Pattern pattern;
		Pattern pattern2;

		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);

			// Get the text content of the PDF
			PDFTextStripper pdfStripper = new PDFTextStripper();
			pdfStripper.setSortByPosition(true);
			pdfStripper.setStartPage(0);
			pdfStripper.setEndPage(document.getNumberOfPages());
			pdfStripper.setLineSeparator("\n");
			pdfStripper.setWordSeparator(" ");
			pdfStripper.setShouldSeparateByBeads(false);

			String text = pdfStripper.getText(document);
			System.out.println(text);
			String lang = detectLanguage(text);

			if (lang.equals("en")) {
				regex = "(?s)EDUCATION.*?(?:(?=\\s*LANGUAGES)|(?=\\s*LICENSES\\s&\\sCERTIFICATIONS)|(?=\\s*CERTIFICATES)|(?=\\s*SKILLS)|(?=\\s*EXPERIENCE)|(?=\\s*PROJECTS)|$)";
				regexLowerCased = "(?s)Education.*?(?:(?=\\s*Languages)|(?=\\s*Licenses\\s&\\sCertifications)|(?=\\s*Certificates)|(?=\\s*skills)|(?=\\s*Experience)|(?=\\s*Projects)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			} else {
				regex = "(?s)FORMATION?.*?(?:(?=\\s*COORDONNÉES)|(?=\\s*CERTIFICATIONS)|(?=\\s*RÉSUMÉ)|(?=\\s*LANGUAGES)|(?=\\s*EXPÉRIENCE)|(?=\\s*PROJETS)|$)";
				regexLowerCased = "(?s)Formation(?:\\s*Techniques)?.*?(?:(?=\\s*Coordonnées)|(?=\\s*Certifications)|(?=\\s*Résumé)|(?=\\s*Languages)|(?=\\s*Expérience)|(?=\\s*Projects)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			}

			Matcher matcher = pattern.matcher(text);
			Matcher matcher2 = pattern2.matcher(text);

			if (matcher.find()) {
				EducationSection = matcher.group();
			} else if (matcher2.find()) {
				EducationSection = matcher2.group();
			}

			document.close();

			return EducationSection;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String extractProjects(MultipartFile file) throws IOException {
		String ProjectSection = null;
		String regex;
		String regexLowerCased;
		Pattern pattern;
		Pattern pattern2;

		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);

			// Get the text content of the PDF
			PDFTextStripper pdfStripper = new PDFTextStripper();
			pdfStripper.setSortByPosition(true);
			pdfStripper.setStartPage(0);
			pdfStripper.setEndPage(document.getNumberOfPages());
			pdfStripper.setLineSeparator("\n");
			pdfStripper.setWordSeparator(" ");
			pdfStripper.setShouldSeparateByBeads(false);

			String text = pdfStripper.getText(document);
			System.out.println(text);
			String lang = detectLanguage(text);

			if (lang.equals("en")) {
				regex = "(?s)PROJECTS.*?(?:(?=\\s*LANGUAGES)|(?=\\s*LICENSES\\s&\\sCERTIFICATIONS)|(?=\\s*CERTIFICATES)|(?=\\s*SKILLS)|(?=\\s*EXPERIENCE)|(?=\\s*EDUCATION)|$)";
				regexLowerCased = "(?s)Projects.*?(?:(?=\\s*Languages)|(?=\\s*Licenses\\s&\\sCertifications)|(?=\\s*Certificates)|(?=\\s*skills)|(?=\\s*Experience)|(?=\\s*Education)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			} else {
				regex = "(?s)PROJETS?.*?(?:(?=\\s*COORDONNÉES)|(?=\\s*CERTIFICATIONS)|(?=\\s*RÉSUMÉ)|(?=\\s*FORMATION)|(?=\\s*LANGUAGES)|(?=\\s*EXPÉRIENCE)|$)";
				regexLowerCased = "(?s)Projets.*?(?:(?=\\s*Coordonnées)|(?=\\s*Certifications)|(?=\\s*Résumé)|(?=\\s*Formation)|(?=\\s*Languages)|(?=\\s*Expérience)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			}

			Matcher matcher = pattern.matcher(text);
			Matcher matcher2 = pattern2.matcher(text);

			if (matcher.find()) {
				ProjectSection = matcher.group();
			} else if (matcher2.find()) {
				ProjectSection = matcher2.group();
			}

			document.close();

			return ProjectSection;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String extractCertifications(MultipartFile file) throws IOException {
		String CertSection = null;
		String regex;
		String regexLowerCased;
		Pattern pattern;
		Pattern pattern2;

		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);

			// Get the text content of the PDF
			PDFTextStripper pdfStripper = new PDFTextStripper();
			pdfStripper.setSortByPosition(true);
			pdfStripper.setStartPage(0);
			pdfStripper.setEndPage(document.getNumberOfPages());
			pdfStripper.setLineSeparator("\n");
			pdfStripper.setWordSeparator(" ");
			pdfStripper.setShouldSeparateByBeads(false);

			String text = pdfStripper.getText(document);
			System.out.println(text);
			String lang = detectLanguage(text);

			if (lang.equals("en")) {
				
				
				regex = "(?s)(?:LICENSES\\s&\\sCERTIFICATIONS|CERTIFICATES).*?(?:(?=\\s*LANGUAGES)|(?=\\s*PROJECTS)|(?=\\s*SKILLS)|(?=\\s*EXPERIENCE)|(?=\\s*EDUCATION)|$)";
				regexLowerCased = "(?s)(?:Licenses\\s&\\sCertifications|Certificates).*?(?:(?=\\s*Languages)|(?=\\s*Projects)|(?=\\s*skills)|(?=\\s*Experience)|(?=\\s*Education)|$)";
				
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			} else {
				regex = "(?s)CERTIFICATIONS?.*?(?:(?=\\s*COORDONNÉES)|(?=\\s*PROJETS)|(?=\\s*RÉSUMÉ)|(?=\\s*FORMATION)|(?=\\s*LANGUAGES)|(?=\\s*EXPÉRIENCE)|$)";
				regexLowerCased = "(?s)Certifications.*?(?:(?=\\s*Coordonnées)|(?=\\s*Projets)|(?=\\s*Résumé)|(?=\\s*Formation)|(?=\\s*Languages)|(?=\\s*Expérience)|$)";
				pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
				pattern2 = Pattern.compile(regexLowerCased, Pattern.CASE_INSENSITIVE);
			}

			Matcher matcher = pattern.matcher(text);
			Matcher matcher2 = pattern2.matcher(text);

			if (matcher.find()) {
				CertSection = matcher.group();
			} else if (matcher2.find()) {
				CertSection = matcher2.group();
			}

			document.close();

			return CertSection;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String detectLanguage(String text) throws IllegalStateException, IOException {
		List<LdLocale> languages = Arrays.asList(LdLocale.fromString("fr"), LdLocale.fromString("en"));
		LanguageProfileReader languageProfileReader = new LanguageProfileReader();
		List<com.optimaize.langdetect.profiles.LanguageProfile> profiles = languageProfileReader.readBuiltIn(languages);
		com.optimaize.langdetect.LanguageDetector languageDetector = LanguageDetectorBuilder
				.create(NgramExtractors.standard()).withProfiles(profiles).build();
		Optional<LdLocale> language = languageDetector.detect(text);
		String detectedLanguage = language.isPresent() ? language.get().getLanguage() : "unknown";
		System.out.println("Detected language: " + detectedLanguage);
		return detectedLanguage;
	}

}
