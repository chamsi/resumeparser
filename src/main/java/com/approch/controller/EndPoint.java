package com.approch.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

//second approach will only work when we using linkedin cvs

@RestController
public class EndPoint {

	@PostMapping("/pdf/text/1")
	public Map<String, String> extractTextFromPDF(@RequestParam("file") MultipartFile file) throws IOException {
		try (InputStream stream = file.getInputStream()) {
			PDDocument document = PDDocument.load(stream);
			PDFTextStripper stripper = new PDFTextStripper();
			String text = stripper.getText(document);
			document.close();

			// Define regular expressions for section headings
			String[] sectionHeadings = new String[] { "Résumé", "Summary", "Expérience", "Experience", "Formation",
					"Education", "Certifications", "Licenses & Certifications", "Skills", "Principales compétences",
					"Compétences", "COMPÉTENCES TECHNIQUES" };

			// Combine the regular expressions into a single pattern

			String pattern = "(?<=" + String.join("|", sectionHeadings) + ")";
			// Split the text into sections based on the pattern
			String[] sections = text.split(pattern);

			Map<String, String> sectionMap = new HashMap<>();
			for (int i = 0; i < sections.length; i++) {
				String section = sections[i].trim();
				for (String heading : sectionHeadings) {
					if (section.toLowerCase().contains(heading.toLowerCase())) {
						if (i + 1 < sections.length) {

							sectionMap.put(heading, sections[i + 1]);

						}
						break;
					}
				}
			}

			String Résumé = sectionMap.get("Résumé");
			if (Résumé != null) {
				String summary = sectionMap.get("Summary");
				if (summary != null) {
					summary += "\n" + Résumé;
					sectionMap.put("Summary", summary);
					sectionMap.remove("Résumé");
				} else {
					sectionMap.put("Summary", Résumé);
				}
			}

			String LCertifications = sectionMap.get("Licenses & Certifications");
			if (LCertifications != null) {
				String Certifications = sectionMap.get("Certifications");
				if (Certifications != null) {
					Certifications += "\n" + LCertifications;
					sectionMap.put("Certifications", Certifications);
					sectionMap.remove("LCertifications");
				} else {
					sectionMap.put("Certifications", LCertifications);
				}
			}

			String formation = sectionMap.get("Formation");
			if (formation != null) {
				String education = sectionMap.get("Education");
				if (education != null) {
					education += "\n" + formation;
					sectionMap.put("Education", education);
					sectionMap.remove("Formation");
				}
			}
			String expérience = sectionMap.get("Expérience");
			if (expérience != null) {
				String experience = sectionMap.get("Experience");
				if (experience != null) {
					experience += "\n" + expérience;
					sectionMap.put("Experience", experience);
					sectionMap.remove("Expérience");
				}
			}

			String competenceP = sectionMap.get("Principales compétences");
			String competence = sectionMap.get("Compétences");
			String CompetenceT = sectionMap.get("COMPÉTENCES TECHNIQUES");
			if (competence != null) {
				String skills = sectionMap.get("Skills");
				if (skills != null) {
					skills += "\n" + competence;
					sectionMap.put("Skills", skills);
					sectionMap.remove("competence");
				} else {
					sectionMap.put("Skills", competence);
				}
			}

			if (CompetenceT != null) {
				String skills = sectionMap.get("Skills");
				if (skills != null) {
					skills += "\n" + CompetenceT;
					sectionMap.put("Skills", skills);
					sectionMap.remove("COMPÉTENCES TECHNIQUES");
				} else {
					sectionMap.put("Skills", CompetenceT);
				}
			}
			if (competenceP != null) {
				String skills = sectionMap.get("Skills");
				if (skills != null) {
					skills += "\n" + competenceP;
					sectionMap.put("Skills", skills);
					sectionMap.remove("Principales compétences");
				} else {
					sectionMap.put("Skills", competenceP);
				}
			}

			return sectionMap;
		} catch (IOException e) {
			throw new IOException("Failed to extract text from PDF", e);
		}
	}
}
